<?php

namespace App\Controller;

use League\OAuth2\Client\Provider\GenericProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index() {

        $session = new Session();
        $session->start();

        return $this->render('index.html.twig', [
            'user' => $session->get('userData')
        ]);

    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout() {

        $session = new Session();
        $session->start();

        $session->remove('userData');
        return $this->redirectToRoute('index', array(), 301);

    }

    /**
     * @Route("/login", name="login")
     */
    public function login() {

        $session = new Session();
        $session->start();

        $provider = new GenericProvider([
            'clientId'                => '5a27cf9bf6d44948ad0d3b41_3nlg7djjw3qc48owkkk48g00k8840444kk4w84s0wwkswwswk4',
            'clientSecret'            => '2xhshkd2e340cswgg0okgs0wogswckw80sc8gocgw4ogo884wk',
            'redirectUri'             => 'http://localhost:8000/login',
            'urlAuthorize'            => 'https://api.ynovweb.heavydev.fr/oauth/v2/auth',
            'urlAccessToken'          => 'https://api.ynovweb.heavydev.fr/oauth/v2/token',
            'urlResourceOwnerDetails' => 'https://api.ynovweb.heavydev.fr/user/me'
        ]);

        if (!isset($_GET['code']))
        {
            $authorizationUrl = $provider->getAuthorizationUrl();
            $session->set('oauth2state', $provider->getState());
            header('Location: ' . $authorizationUrl);
            exit();
        }
        elseif (empty($_GET['state']) || ($_GET['state'] !== $session->get('oauth2state')))
        {
            $session->remove('oauth2state');
            exit('Invalid state');
        }
        else
        {
            try
            {
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);

                $resourceOwner = $provider->getResourceOwner($accessToken);
                $session->set('userData', $resourceOwner->toArray());
                return $this->redirectToRoute('index', array(), 301);
            }
            catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e)
            {
                exit($e->getMessage());
            }
        }
    }
}
