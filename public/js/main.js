// Global declarations
var systems = ["Windows", "MacOS", "Linux"];
var browser = navigator.appVersion;

// Detect client OS
if (browser.indexOf("Win") != -1) OSName = systems[0];
else if (browser.indexOf("Mac") != -1) OSName = systems[1];
else if (browser.indexOf("X11") != -1 || browser.indexOf("Linux") != -1) OSName = systems[2];
else OSName = "Unknown";

if(OSName != "Unknown") {
    document.getElementById("version").innerHTML = OSName;
    document.querySelector(".btn-link").setAttribute("href", /*"dl/test_" + OSName.toLowerCase() + ".txt"*/"dl/ingesup.rdp");
    document.querySelector(".btn-img").setAttribute("src", "img/" + OSName.toLowerCase() + ".png");

    if(OSName == "MacOS") {
        document.getElementById("instructions2").style.display = "none";
        document.getElementById("instructions1").style.display = "table-row";
        document.getElementById("macOption").style.display = "table-cell";
    }
}

document.querySelector('.link-others').addEventListener('click', function() {
    toggleOthers();
});

// Show others OS
function toggleOthers() {
    var othersDiv = document.querySelector('.others');

    othersDiv.innerHTML = '';
    systems.forEach(function(value, index) {
        if(value != OSName) {
            othersDiv.innerHTML += '<a href="dl/test_'
            + value.toLowerCase() + '.txt" download><img src="img/'
            + value.toLowerCase() + '.png" title="' + value + '"></a>';
        }
    });

    othersDiv.classList.toggle('fade');
}
